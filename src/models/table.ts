import * as wjCore from "@grapecity/wijmo";

interface Cell {
  binding: string;
  header: string;
  cssClass?: string;
  format?: string;
  isReadOnly?: boolean;
  wordWrap?: boolean;
  width: number;
}

export interface AppData {
  orders: wjCore.CollectionView;
  groupedOrders: wjCore.CollectionView;
  pagedOrders: wjCore.CollectionView;
  addNewOrders: wjCore.CollectionView;
  ldOneLine: Cell[];
  ldTwoLines: Line[];
  ldThreeLines: Line[];
  layoutDefs: wjCore.CollectionView;
}

interface Line {
  Cells: Cell[];
  header?: string;
  colspan?: number;
}
