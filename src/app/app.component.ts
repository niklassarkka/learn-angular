import { Component, OnInit, Directive} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  validateUserName = validateUserName;
  formGroup =  this.fb.group({
    username: ['622', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]],
    password: ['ssss', [Validators.required, Validators.pattern(REGEX_PASSWORD)]],
    email: ['', [Validators.required, Validators.email]]
  });
  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
  }

  getControl(pzKey: string): AbstractControl{
    return <AbstractControl>this.formGroup.get(pzKey);
  }
}

const REGEX_PASSWORD = /^(?=.*\d)(?=.*[a-zA-Z]).{6,50}$/;

const validateUserName = [
  {zType: 'required', zMessage: 'Không được để trống username'},
  {zType: 'minlength', zMessage: 'Không ít hơn 3 ký tự'},
  {zType: 'maxlength', zMessage: 'Giới hạn 10 ký tự'},
]
