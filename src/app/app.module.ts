import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { InputLoginComponent } from './login/input-login/input-login.component';
import {ReactiveFormsModule} from "@angular/forms";
import { GridComponent } from './grid/grid.component';
import { WjGridModule } from '@grapecity/wijmo.angular2.grid';

@NgModule({
  declarations: [
    AppComponent,
    InputLoginComponent,
    GridComponent
  ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        WjGridModule,
        BrowserModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
